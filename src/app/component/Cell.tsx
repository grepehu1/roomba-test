import { useEffect, useState } from "react"
import { useHandStore, useObstaclesStore } from "../stores/hand"


export default function Cell({
    x, y
}: {
    x: number,
    y: number,
}) {
    const posX = useHandStore((state) => state.posX)
    const posY = useHandStore((state) => state.posY)
    const orientation = useHandStore((state) => state.orientation)

    const obstables = useObstaclesStore((state) => state.obstacles)

    const [isObstacle, setIsObstacle] = useState(false)

    useEffect(() => {
        const currentObstacle = obstables.find(thisObs => {
            return x === thisObs.posX && y === thisObs.posY
        })
        setIsObstacle(!!currentObstacle)
    }, [obstables])


    return (
        <>
            {isObstacle ? (
                <div className="h-full w-full bg-black"></div>
            ) : x === posX && y === posY ? (
                <div className={`text-sm hand-icon hand-${orientation}`}>☝🏼</div>
            ) : <></>}
        </>
    )
}
