'use client'

import { KeyboardEvent, useCallback } from "react"
import Cell from "./component/Cell"
import { useHandStore, useObstaclesStore } from "./stores/hand"

export default function Home() {
  const rotate = useHandStore((state) => state.rotate)
  const moveForward = useHandStore((state) => state.moveForward)
  const moveByArrow = useHandStore((state) => state.moveByArrow)

  const obstables = useObstaclesStore((state) => state.obstacles)

  const handleKeydown = useCallback(
    (e: KeyboardEvent<HTMLElement>) => {
      const key = e.key
      moveByArrow(key, obstables)
    },
    [obstables],
  )

  return (
    <main tabIndex={0} onKeyUp={handleKeydown} className="flex flex-col gap-2" >
      <div className="flex gap-2">
        <button className="p-2 bg-green-500 hover:opacity-50 text-black" onClick={() => rotate()}>Turn Right</button>
        <button className="p-2 bg-green-500 hover:opacity-50 text-black" onClick={() => moveForward(obstables)}>Move Forward</button>
      </div>
      <div className="Grid">
        {Array.from(Array(10).keys()).map(colX => (
          <div key={`col-${colX}`} className="Column">
            {Array.from(Array(10).keys()).map(colY => (
              <div key={`cell-${colY}`} className="Cell flex justify-center items-center">
                <Cell x={colX} y={colY} />
              </div>
            ))}
          </div>
        ))}
      </div>
    </main >
  )
}
