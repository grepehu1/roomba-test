import { create } from 'zustand'

type Obstacle = {
    posX: number,
    posY: number,
}

interface CurrentObstacles {
    obstacles: Obstacle[]
}


export const useObstaclesStore = create<CurrentObstacles>(set => ({
    obstacles: [
        { posX: 2, posY: 8 },
        { posX: 5, posY: 7 },
        { posX: 8, posY: 3 },
    ]
}))

type Hand = {
    posX: number,
    posY: number,
    orientation: number,
    rotate: () => void,
    moveForward: (obstables: Obstacle[]) => void,
    moveByArrow: (key: string, obstables: Obstacle[]) => void;
}

function isObstacle(obstables: Obstacle[], x: number, y: number) {
    return !!obstables.find(thisObs => {
        return x === thisObs.posX && y === thisObs.posY
    })
}

function manualRotate(state: Hand) {
    const newHand: Hand = { ...state }
    newHand.orientation = (state.orientation + 90) % 360
    return newHand
}

export const useHandStore = create<Hand>((set) => ({
    posX: 0,
    posY: 0,
    orientation: 0,
    rotate: () => set(manualRotate),
    moveForward: (obstables: Obstacle[]) => set((state) => {
        let newHand: Hand = { ...state }
        switch (state.orientation) {
            case 0:
                if (!newHand.posY || isObstacle(obstables, newHand.posX, newHand.posY - 1)) {
                    newHand = manualRotate(newHand)
                } else {
                    newHand.posY -= 1
                }
                break;
            case 90:
                if (newHand.posX >= 9 || isObstacle(obstables, newHand.posX + 1, newHand.posY)) {
                    newHand = manualRotate(newHand)
                } else {
                    newHand.posX += 1
                }
                break;
            case 180:
                if (newHand.posY >= 9 || isObstacle(obstables, newHand.posX, newHand.posY + 1)) {
                    newHand = manualRotate(newHand)
                } else {
                    newHand.posY += 1
                }
                break;
            case 270:
                if (!newHand.posX || isObstacle(obstables, newHand.posX - 1, newHand.posY)) {
                    newHand = manualRotate(newHand)
                } else {
                    newHand.posX -= 1
                }
                break;
        }
        return newHand
    }),
    moveByArrow: (key: string, obstables: Obstacle[]) => set((state) => {

        let newHand: Hand = { ...state }
        switch (key) {
            case 'ArrowUp':
                if (!newHand.posY || isObstacle(obstables, newHand.posX, newHand.posY - 1)) {
                    newHand = manualRotate(newHand)
                } else {
                    newHand.orientation = 0
                    newHand.posY -= 1
                }
                break;
            case 'ArrowRight':
                if (newHand.posX >= 9 || isObstacle(obstables, newHand.posX + 1, newHand.posY)) {
                    newHand = manualRotate(newHand)
                } else {
                    newHand.orientation = 90
                    newHand.posX += 1
                }
                break;
            case 'ArrowDown':
                if (newHand.posY >= 9 || isObstacle(obstables, newHand.posX, newHand.posY + 1)) {
                    newHand = manualRotate(newHand)
                } else {
                    newHand.orientation = 180
                    newHand.posY += 1
                }
                break;
            case 'ArrowLeft':
                if (!newHand.posX || isObstacle(obstables, newHand.posX - 1, newHand.posY)) {
                    newHand = manualRotate(newHand)
                } else {
                    newHand.orientation = 270
                    newHand.posX -= 1
                }
                break;
        }
        return newHand
    }),
}))