import Home from "../app/page";
import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";

describe("Home page", () => {
    it("renders home", () => {
        render(<Home />);

        expect(screen.getByText('home')).toBeInTheDocument();
    });
});