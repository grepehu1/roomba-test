# Roomba

Create an application that moves a Roomba around a 10x10 grid.

## Exercise

Create separate buttons that command the Roomba to either:

1. Turn 90 degrees right
2. Move forward

## Layout

- The Roomba moves through a 10x10 grid
Interaction
- Roomba cannot pass through the walls
- If Roomba hits a wall, Roomba will turn 90 degrees right
- When the Roomba enters a cell, it continues to point in the same direction it did in the previous cell HTML & CSS You can use the following HTML & CSS to represent the grid:

## HTML

```html
<div class="Grid">
    <div class="Column">
        <div class="Cell"></div>
        <!-- X 10 -->
    </div>
    <!-- X 10 -->
</div>
```

## CSS

```css
.Grid {
    border: 2px solid black;
    height: 400px;
    width: 400px;
    display: flex;
    flex-direction: row;
}
.Column {
    display: flex;
    flex-direction: column;
    flex: 1;
}
.Cell {
    flex: 1;
    border: 1px solid black;
    text-align: center;
    font-size: 2em;
}
```

## Development environment

You are welcome to use the third-party view framework of your choice, or no framework
at all. Examples of view frameworks: Vue.js, React, Angular.